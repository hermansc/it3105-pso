\documentclass[a4paper]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{todonotes}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}

\title{Artificial Intelligence Programming\\
    \normalsize Particle Swarm Optimization}
\author{Herman Schistad - MTDT}
\date{24.11.2013}
\begin{document}
\pagenumbering{roman}
\maketitle

\section{Solving the circle problem}

As our first challenge we will use a particle swarm to solve the circle
problem, which is defined as:

$f(u_1,\dots,u_x) = (u_1 * u_1) + \dots + (u_x * u_x)$

$x$ is a number defining the number of dimensions to solve for. So if $x=2$,
then we have:

$f(2,3) = (2*2) + (3*3) = 4 + 9 = 12$

We, however want to minimize this value in such a way that the global fitness
(the answer of the function above) is less than $0.001$.

We do this by generating 60 particles each with a \textit{random position and
velocity}. Then we calculate a fitness score for each particle (the fitness
score in this task just being the result of the function defined above). As we
want to minimize the fitness globally, all particles check if the fitness score
they obtained in their current position is smaller than the global minimum.
They also keep track of the best position they've been in locally. Both these
numbers (best local and global fitness) will then be used to calculate a new
velocity.

After all particles has calculated their scores (and we thus have a global
minimum) we calculate new velocities for each particle. The formula to do so is
defined as follows:

$v(t + 1) = v(t) + (c_1 * r_1 * (p(t) - x(t))) + (c_2 * r_2 * (g(t) - x(t)))$

Where $r_1$ and $r_2$ are random values in the range $[0,1)$, $c_1$ and $c_2$
are weights for the local and global attraction, respectively. $g(t)$ is the
best global position at time $t$. $p(t)$ is the best position seen for the
current particle and $x(t)$ is the current position for the particle in
question.

Using the new velocity we update the position of each particle with the
following simple formula:

$x(t + 1) = x(t) + v(t)$

When all particles have obtained their new positions, we do the same procedure
again until either the best global fitness is below our fitness goal or the
number of iterations are above our pre-defined number.

Thus a simple pseudo-code looks like:

\begin{verbatim}
particles <- generate 60 particles with random positions and velocities

while best fitness seen is below goal and iterations are below max iterations:
    for each particle in particles:
        for each dimension in particle:
            calculate new velocity
            calculate new position
        calculate particle fitness 

        if particle fitness < best global fitness seen:
            best fitness seen <- particle fitness
            best positions seen <- particle positions
print best fitness seen
\end{verbatim}

I use the word 'global' fitness to describe the best fitness seen across all
particles, thus the particles are fully connected / aware of all other
particles. Later we will use a KNN approach, and then $g(t)$ is "unique" for
each particle, and not the global best positions.

But in this task, we thus run the algorithm in order to solve the 1D circle
problem and get the following results (with 10 particles) and a maximum of 100
iterations.

\begin{verbatim}
------------------------------------------------------------
Solving circle problem using:
10 particles, with KNN=0 and 1 dimensions
Maximum 100 iterations and fitness goal: 0.001
------------------------------------------------------------

Found solution on iteration 3
Best fitness: 2.1823340916739857e-05
Best particle position [-0.0046715458808342936]

Found solution on iteration 3
Best fitness: 0.000801223787346957
Best particle position [-0.028305896688622267]

Found solution on iteration 12
Best fitness: 2.51983035250241e-05
Best particle position [-0.00501979118340834]

Found solution on iteration 3
Best fitness: 0.0005773325065391078
Best particle position [-0.024027744516269267]

Found solution on iteration 4
Best fitness: 0.0002806226818825246
Best particle position [0.016751796377777656]

Found solution on iteration 6
Best fitness: 0.0004438221152553524
Best particle position [0.021067086064649576]

Found solution on iteration 6
Best fitness: 0.0006681661228871087
Best particle position [0.025848909510598483]

Found solution on iteration 7
Best fitness: 0.0004224895563660663
Best particle position [0.020554550745907008]

Found solution on iteration 4
Best fitness: 0.000827626187716331
Best particle position [0.02876849296915518]

Found solution on iteration 1
Best fitness: 0.00012707457136763048
Best particle position [0.011272735753473088]
\end{verbatim}

Or in table-form:

\begin{table}[h!]
    \begin{tabular}{|c|c|c|}
        \hline
        Solution found on iteration & Best fitness & Particle position \\
        \hline
        3   & 2.18235e-05 & -0.00467154 \\
        3   & 0.000801223 & -0.02830589 \\
        12  & 2.51983e-05 & -0.00501979 \\
        3   & 0.000577332 & -0.02402774 \\
        4   & 0.000280622 &  0.01675179 \\
        6   & 0.000443822 &  0.02106708 \\
        6   & 0.000668166 &  0.02584890 \\
        7   & 0.000422489 &  0.02055455 \\
        4   & 0.000827626 &  0.02876849 \\
        1   & 0.000127074 &  0.01127273 \\ \hline
    \end{tabular}
\end{table}

As one can see, the solution is found very quick. This is not surprising, as
the problem is a very simple one and we only have one dimension to optimize.
When the solution is found on the first iteration we've been "lucky" and the
particle position upon generation yields a solution to the problem, without
calculating a new velocity.

Here is a plot showing the coordinates of 40 particles for the same problem:

\includegraphics[scale=0.5]{circle-1d}

Now we extend the problem to solve the 2d circle problem. The program works
just as before, but now when we calculate the fitness we need to take into
account both dimensions. The result looks like this (using 40 particles):

\begin{verbatim}
------------------------------------------------------------
Solving circle problem using:
40 particles, with KNN=0 and 2 dimensions
Maximum 100 iterations and fitness goal: 0.001
------------------------------------------------------------

Found solution on iteration 5
Best fitness: 0.00037159598082897794
Best particle position [-0.012243755902116016 -0.014889137726489643]

Found solution on iteration 14
Best fitness: 0.0003440699330969024
Best particle position [-0.007969487881670623 0.016749841670917594]

Found solution on iteration 5
Best fitness: 0.00016888416218146965
Best particle position [0.0004641556555720974 -0.012987252277093492]

Found solution on iteration 11
Best fitness: 5.9306668145418655e-05
Best particle position [0.0035365096221030567 0.006841035582292432]

Found solution on iteration 11
Best fitness: 0.000257511706445182
Best particle position [0.01563692392657079 -0.0036053178167562427]

Found solution on iteration 7
Best fitness: 0.0008324424278545524
Best particle position [-0.004290459234756516 -0.028531287868048386]

Found solution on iteration 8
Best fitness: 0.0008884775109326996
Best particle position [0.02049236743958116 0.021645793763544304]

Found solution on iteration 19
Best fitness: 0.00033790472530283356
Best particle position [-0.007239076025386149 -0.016896760151032298]

Found solution on iteration 11
Best fitness: 0.0007496820844468397
Best particle position [-0.027363404056753887 -0.0009623943441566252]

Found solution on iteration 25
Best fitness: 3.062078432916799e-05
Best particle position [-0.0049288735295636155 0.002515350881832412]
\end{verbatim}

Or the same numbers, concentrated to a table-form:

\begin{table}[h!]
    \begin{tabular}{|c|c|c|}
        \hline
        Solution found on iteration & Best fitness & Particle position \\
        \hline
        5    & 0.00037159 & [-0.012243755 -0.014889137] \\
        14   & 0.00034406 & [-0.007969487  0.016749841] \\
        5    & 0.00016888 & [ 0.000464155 -0.012987252] \\
        11   & 5.9306e-05 & [ 0.003536509  0.006841035] \\
        11   & 0.00025751 & [ 0.015636923 -0.003605317] \\
        7    & 0.00083244 & [-0.004290459 -0.028531287] \\
        8    & 0.00088847 & [ 0.020492367  0.021645793] \\
        19   & 0.00033790 & [-0.007239076 -0.016896760] \\
        11   & 0.00074968 & [-0.027363404 -0.000962394] \\
        25   & 3.0620e-05 & [-0.004928873  0.002515350] \\ \hline
    \end{tabular}
\end{table}

Here is a plot show one example of how the particles are spread out using the
same 40 particles as above for the 2D circle problem:

\includegraphics[scale=0.5]{circle-2d}

\clearpage
\section{Extending PSO with KNN and inertia weights}

Until now we've solved the PSO where all particles are fully connected, but
this has one problem: it is easy to find local minima, and not necessarily the
best global fitness. We can solve this by comparing the distances between the
different particles and updating the velocity based on the $n$ closest
neighbours. How we calculate the distances (and thus find neighbours) are up
to the programmer. In my implementation the distance calculation is done either
by Manhattan distance or euclidean distance. The latter being more taking
more processing to calculate.

The Manhattan distance between two position vector arrays is calculated as:

$d(p,q) = || p - q || = \sum _{i=1}^{n} |p_i - q_i|$

And the euclidean distance is found by:

$d(p,q) = \sqrt{\sum _{i=1}^{n} (q_i - p_i)^2}$

A run using 10 particles, $c1 = 1.0$, $c2 = 1.0$ and solving the 1d and 2d
problem looks like this:

\includegraphics[scale=0.5]{circle-knn}

We can also add an inertia to the velocity calculation (inertia meaning
amount of resistance to change in velocity). In the velocity calculation we
multiply the current velocity with an inertia factor, starting at 1.0 and
decreasing to 0.4. This means that the particle is more welcoming to velocity
change after more iterations. This can both have good and bad effects: if we
are too welcoming we may never find a good solution, as we are changing the
velocity too much - but if we are too resistant we will never find a solution
within a reasonable time. In my experiments, and as one will see below in task
3 and 4, a inertia factor of ~0.7 proved good, and in a future implementation
one should perhaps consider a normally distributed inertia factor with a mean of
0.7.

The function to update the velocity then becomes:
$v(t + 1) = (w * v(t)) + (c_1 * r_1 * (p(t) - x(t))) + (c_2 * r_2 * (g(t) - x(t)))$

here $w$ denoting the inertia factor and the rest of the function the same as
described in task 1.

The result of using KNN=3, inertia and the same parameters as above we get the
following best global solutions with PSO:

\includegraphics[scale=0.5]{circle-knn-inertia}

\clearpage
\section{Using PSO to solve the 0-1 Knapsack problem}

The circle problem is a simple problem, especially for as few dimensions as
we've seen so far. It shows however how we can elegantly solve complex
problems, many of these often called NP-complete problems. One such problem is
the 0-1 knapsack problem, where we have $n$ packages, each having a value and
weight and we want to maximize the total value and weight inside a backpack
having a limit of $x$ kg. In a NP-complete problem the solution is hard to
find, but easy to verify, and the same is true for this problem.

We are provided with a text file containing 2000 packages with their values
and weights. Using the PSO we can generate ~4000 particles with random
positions and velocities between 0 and 1. Then if the position is above a
certain threshold we say the package is included in the backpack, and if it is
below, we do not include it. In my implementation I've set this threshold to
0.85. I also limit both the position and velocity, in such a way that I can
guarantee that a particle does not "fly away". This have the consequence of in
some settings, finding solutions slower, but in other states it can help us not
having to search unnecessary package combinations. The velocity is limited at
-0.1 and 0.1. Hence if the new velocity calculated for a particle is 0.2, the
algorithm considers this speed to high, and limits it to 0.1.
In the same manner we limit the position, in such a way that is never goes
below 0.0 or above 1.0.

Our fitness function however is more complex in this task, compared to the
circle problem, and may have a large impact on the performance of the
algorithm. We want to minimize the fitness, so I've chosen the following
fitness function, here in pseudo-code:

\begin{verbatim}
if current weight > max weight:
  fitness = max weight + current weight
else:
  fitness = - current value
\end{verbatim}

Here we see that in the case of a valid solution we have negative fitness,
while if we have a invalid solution the fitness is very large. In a more
complex fitness function one could give more penalty solutions farther away
from the solution and/or less penalty to solutions very close to the solution
(but perhaps having one package too much).

The local (c1) and global (c2) attraction rates also makes an impact on the
solution, and I've here included three results. One where $c1 = 1.0$ and $c2 =
1.0$, another with $c1 = 1.2$ and $c2 = 0.8$ and the last one with $c1 = 0.8$
and $c2 = 1.2$.

I'm having twice as many particles as I have dimensions, thus in the examples
below I'm using 4000 particles. The particles are fully connected.

As we do not know what the optimal solution is we can not easily predict what
value we should set as our goal. In order to not quit the algorithm before
finding the optimal solution we set the fitness goal to an large negative
number, such as the negative of the max weight.

First let us take a look at the result when we use 1.0 as our inertia weight:

\begin{table}[h!]
    \begin{tabular}{|c|c|c|}
    \hline
    c1   & c2  & Optimal solution found after 500 iterations \\ \hline
    1.0  & 1.0 & 221.64 \\
    1.2  & 0.8 & 346.05 \\
    0.8  & 1.2 & 0      \\ \hline
    \end{tabular}
\end{table}

For $c1 = 0.8$ and $c2 = 1.2$ we don't even find a solution. And the other
solutions are not very impressive either. By looking at the plot we see that
the best global fitness are not changing very frequently, which corresponds
well with our inertia weight: the particles uses too long time to "change"
speed and their current velocity makes too large of an impact on the velocity
calculation. 

\includegraphics[scale=0.5]{pso-non-inertia}

We need more lean particles. So we use the same method as before,
we start with inertia at 1.0 and slowly reduce it to 0.4 at the end of the
program. We then obtain the following results:

\begin{table}[h!]
    \begin{tabular}{|c|c|c|}
    \hline
    c1   & c2  & Optimal solution found after 500 iterations \\ \hline
    1.0  & 1.0 & 382.5460 \\
    1.2  & 0.8 & 442.2456 \\
    0.8  & 1.2 & 392.0377 \\ \hline
    \end{tabular}
\end{table}

By looking at the plot we see how the optimal value found develops after a
certain amount of iteration. As one can see no combination of c1 and c2 finds
any new solutions after iteration 300, and this probably, among other factors,
because the inertia becomes too low.

\includegraphics[scale=0.5]{pso-chart}

\clearpage
\section{Solving the 0-1 Knapsack problem with additional constraints}

With PSO above, adding another constraint is not very difficult. We modify our
fitness function to also consider penalizing solutions having a total volume
above the max volume allowed. The volumes are generated randomly between 1 and
100 for each package, and we solve the problem using 1.0 for both $c1$ and $c2$.

The fitness function defined for this task is as follows:
\begin{verbatim}
fitness = 0

if (current weight > max weight) or (current volume > max volume):

    if current weight > max weight:
        fitness += max weight + current weight

    if current volume > max volume:
        fitness += max volume + current volume

else:
    fitness = -(current value)
return fitness
\end{verbatim}

This is intuitive and simple. If a package combination has both too much weight
and too large volume, then the fitness is very high (remember we want to
minimize the fitness).
\end{document}
