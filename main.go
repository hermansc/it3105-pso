package main

import (
    "fmt"
    "time"
    "math"
    "strings"
    "io/ioutil"
    "math/rand"
    "log"
    "strconv"
    "flag"
    "runtime/pprof"
    "os"
    "github.com/sbinet/go-gnuplot"
)

type Particle struct {
    positions []float64
    velocities []float64
    bestPositions []float64
    bestFitness float64

    // For knapsack problem
    currentWeight float64
    currentValue float64
    currentVolume float64
    bestWeight float64
    bestValue float64
    bestVolume float64
}

type Problem struct {
    dimensions int
    score func(*Particle) float64
    fitnessGoal float64
    maxIterations int
    particles int
    knn int
    threshold float64 // Only used in knapsack
    maxWeight float64 // Only used in knapsack
    maxVolume float64 // Only used in knapsack
    c1 float64 // Local attraction
    c2 float64 // Gloval attraction
    inertia func(int) float64
    updatePosition func(*Particle,int,float64)
    updateVelocity func(*Particle,int,float64)
    upperbounds []float64
    lowerbounds []float64
}

var currentProblem *Problem
var weights []float64
var values []float64
var volumes []float64

func knapsackInertia(iteration int) float64 {
    return 20.0
}

func defaultInertia(iteration int) float64 {
    // between 1 and 0.4
    in := 1 - (0.6 * (float64(iteration) / float64(currentProblem.maxIterations - 1)))
    return in
}

func readProblemFile(filename string) {
    // Read the file.
    fh, err := ioutil.ReadFile(filename)
    if (err != nil) {
        log.Fatal("Could not read packages file. ", err.Error())
    }

    // Split the file on new lines.
    lines := strings.Split(string(fh), "\n")

    // Parse the file.
    values = make([]float64, len(lines)-1)
    weights = make([]float64, len(lines)-1)
    volumes = make([]float64, len(lines)-1)
    for i, line := range lines[:len(lines)-1] {
        properties := strings.Split(line, ",")
        value, _ := strconv.ParseFloat(strings.Trim(properties[0], "\n\r "), 64)
        weight, err := strconv.ParseFloat(strings.Trim(properties[1], "\n\r "), 64)
        if (err != nil) {
            log.Fatal("Error: ", err.Error())
        }
        values[i] = value
        weights[i] = weight
        volumes[i] = (rand.Float64() * 99) + 1 // Random number between 1 and 100.
    }
}

func calcFuncScore(p *Particle) float64 {
    // We have two dimensions, x and y.
    score := 0.0
    posX := p.positions[0]
    posY := p.positions[1]
    score += math.Pow(2.8125 - posX + (posX * math.Pow(posY, 4)), 2.0)
    score += math.Pow(2.25 - posX + (posX * math.Pow(posY, 2.0)), 2.0)
    score += math.Pow(1.5 - posX + (posX * posY), 2.0)
    return score
}

func calcCircleScore(p *Particle) float64 {
    fitness := 0.0
    for _, pos := range p.positions {
        fitness += pos * pos
    }
    if fitness < p.bestFitness {
        p.bestFitness = fitness
        _ = copy(p.bestPositions, p.positions[0:])
    }
    return fitness
}

func calcKnapsackScore(p *Particle) float64 {
    fitness := 0.0
    // Calculate the weight and values.
    p.currentWeight = 0.0
    p.currentValue = 0.0
    for dim := 0; dim < currentProblem.dimensions; dim++ {
        if (p.positions[dim] > currentProblem.threshold) {
            p.currentWeight += weights[dim]
            p.currentValue += values[dim]
        }
    }

    /*
    penalty := 0.0
    if p.currentWeight > currentProblem.maxWeight {
        penalty = (p.currentWeight - currentProblem.maxWeight)/40000
    }
    fitness = (10/p.currentValue) + penalty
    */
    if (p.currentWeight > currentProblem.maxWeight) {
        fitness = currentProblem.maxWeight + p.currentWeight
    } else {
        // We have a valid solution, now check if the value is new "record"
        fitness = -p.currentValue
    }
    if fitness < p.bestFitness {
        p.bestFitness = fitness
        _ = copy(p.bestPositions, p.positions[0:])
        p.bestValue = p.currentValue
        p.bestWeight = p.currentWeight
    }
    return fitness
}

func calcKnapsackVolumeScore(p *Particle) float64 {
    p.currentWeight = 0.0
    p.currentValue = 0.0
    p.currentVolume = 0.0

    // Count weight, value and volume
    for dim := 0; dim < currentProblem.dimensions; dim++ {
        // This package is included in the container.
        if (p.positions[dim] > currentProblem.threshold) {
            p.currentWeight += weights[dim]
            p.currentValue += values[dim]
            p.currentVolume += volumes[dim]
        }
    }

    fitness := 0.0
    if (p.currentWeight > currentProblem.maxWeight || p.currentVolume > currentProblem.maxVolume) {
        if (p.currentWeight > currentProblem.maxWeight) {
            fitness += currentProblem.maxWeight + p.currentWeight
        }
        if (p.currentVolume > currentProblem.maxVolume) {
            fitness += currentProblem.maxVolume + p.currentVolume;
        }
    } else {
        fitness = -p.currentValue
    }
    if fitness < p.bestFitness {
        p.bestFitness = fitness
        _ = copy(p.bestPositions, p.positions[0:])
        p.bestValue = p.currentValue
        p.bestWeight = p.currentWeight
        p.bestVolume = p.currentVolume
    }
    return fitness
}

func knapsackProblemDef(p *Problem, knapsackFile string) *Problem {
    // Ensure we read the problem file.
    if knapsackFile != "" {
        readProblemFile(knapsackFile)
    } else {
        readProblemFile("pso-packages-extra-small.txt")
    }

    // Dimensions is number of lines in the problem file.
    p.dimensions = len(values)

    // Number of particles is 2x the number of dimensions.
    p.particles = 2 * p.dimensions

    // Max weight in knapsack is 1000.
    p.maxWeight = 1000

    // Max volume that the container can hold.
    p.maxVolume = 1000

    p.threshold = 0.85

    // Fitness goal is negative of goal weight.
    p.fitnessGoal = -p.maxWeight

    p.maxIterations = 1000

    // Score function for this problem.
    // Maximizing the value, while in the same time penalizing too high weights.
    p.score = calcKnapsackScore

    //p.inertia = knapsackInertia
    p.inertia = defaultInertia

    p.c1 = 1.0
    p.c2 = 1.0

    p.updateVelocity = func(p *Particle, dim int, newVel float64) {
        // Limit the velocity 
        rate := 0.1
        if (newVel > rate) {
            p.velocities[dim] = rate
        } else if (newVel < -rate) {
            p.velocities[dim] = -rate
        } else {
            p.velocities[dim] = newVel
        }
    }

    p.updatePosition = func(p *Particle, dim int, newVel float64) {
        p.positions[dim] += newVel
        // Limit the particle position.
        if (p.positions[dim] > 1.0) {
            p.positions[dim] = 1.0
        } else if (p.positions[dim] < 0.0) {
            p.positions[dim] = 0.0
        }
    }
    return p
}

func getProblem(problemName string, knapsackFile string) *Problem {
    p := &Problem {
        particles: 60,
        dimensions: 2,
        fitnessGoal: 0,
        maxIterations: 1000,
        knn: 0,
        c1: 1.0,
        c2: 1.0,
        inertia: defaultInertia,
        updateVelocity: func(p *Particle, dim int, newVel float64) {
            p.velocities[dim] = newVel
        },
        updatePosition: func(p *Particle, dim int, newVel float64) {
            p.positions[dim] += newVel
        },
    }
    switch problemName {
        case "knapsack":
            p = knapsackProblemDef(p, knapsackFile)
        case "knapsack-volume":
            p = knapsackProblemDef(p, knapsackFile)
            p.score = calcKnapsackVolumeScore
        case "func":
            // Simple function calculation.
            p.score = calcFuncScore
            p.upperbounds = []float64{4.0, 1.0}
            p.lowerbounds = []float64{1.0, -1.0}
        case "circle":
            // Fitness goal defined in exercice.
            p.fitnessGoal = 0.001

            // Score that calculates square of position.
            p.score = calcCircleScore

            p.knn = 0
    }
    return p
}

func CreateParticle() *Particle {
    p := &Particle{
        positions: make([]float64, currentProblem.dimensions),
        velocities: make([]float64, currentProblem.dimensions),
        bestPositions: make([]float64, currentProblem.dimensions),
    }

    for dim := 0; dim < currentProblem.dimensions; dim++ {
        if len(currentProblem.upperbounds) > 0 {
            lower := currentProblem.lowerbounds[dim]
            upper := currentProblem.upperbounds[dim]
            p.positions[dim] = rand.Float64() * (upper - lower) + lower
            p.velocities[dim] = (rand.Float64() * 2) - 1
        } else {
            p.positions[dim] = rand.Float64()
            p.velocities[dim] = rand.Float64()
        }

        // Initialize the best seen position to the start state
        p.bestPositions[dim] = p.positions[dim]
    }
    p.bestFitness = currentProblem.score(p)
    p.bestValue = math.MaxFloat64
    return p
}

func GetBestGlobalScores(particles []*Particle) []float64 {
    /* Get the best global score among all particles */
    bestGlobals := make([]float64, currentProblem.dimensions)
    bestGlobalScore := math.MaxFloat64
    for _, particle := range particles {
        if particle.bestFitness < bestGlobalScore {
            bestGlobalScore = particle.bestFitness
            _ = copy(bestGlobals, particle.bestPositions[0:])
        }
    }
    return bestGlobals
}

func newVelocity(inertia, currentVelocity, currentPos, particleBest, globalBest float64) float64 {
    localDiff := particleBest - currentPos
    globalDiff := globalBest - currentPos
    t1 := currentProblem.c1 * rand.Float64() * localDiff
    t2 := currentProblem.c2 * rand.Float64() * globalDiff
    return (inertia * currentVelocity) + t1 + t2
}

func largestElement(array []float64) (float64, int) {
    largest := -(math.MaxFloat64)
    index := -1
    for i, elem := range array {
        if elem > largest {
            largest = elem
            index = i
        }
    }
    return largest, index
}

func euclidianDistance(from *Particle, to *Particle, dimensions int) float64 {
    distance := 0.0
    for dim := 0; dim < dimensions; dim++ {
        localDistance := math.Pow(from.positions[dim] - to.positions[dim], 2.0)
        distance += localDistance
    }
    distance = math.Sqrt(distance)
    return distance
}

func manhattanDistance(from *Particle, to *Particle, dimensions int) float64 {
    distance := 0.0
    for dim := 0; dim < dimensions; dim++ {
        localDistance := from.positions[dim] - to.positions[dim]
        distance += localDistance
    }
    return distance
}

func (p *Particle) getNeighbours(particles []*Particle, n int, distanceType string) []*Particle {
    /* FIXME: Calculates all distances, always. Should be done only once */
    neighbours := make([]*Particle, n)
    distances := make([]float64, n)
    // Fill distances with large values
    for i, _ := range distances {
        distances[i] = math.MaxFloat64
    }

    // Calculate euclidian distance to all other particles.
    for _, particle := range particles {
        // Distance calculation
        distance := 0.0
        if (distanceType == "manhattan") {
            distance = manhattanDistance(p, particle, currentProblem.dimensions)
        } else {
            distance = euclidianDistance(p, particle, currentProblem.dimensions)
        }

        // Is it a close neighbour?
        largest, index := largestElement(distances)
        if (distance < largest) {
            distances[index] = distance
            neighbours[index] = particle
        }
    }
    return neighbours
}

func binaryArray(array []float64, threshold float64) []float64 {
    s := make([]float64, len(array))
    for i, elem := range array {
        if (elem > threshold) { s[i] = 1.0 } else { s[i] = 0.0 }
    }
    return s
}

func getProblemStatement(problemName string, p *Problem) string {
    s := strings.Repeat("-", 60) + "\n"
    s += fmt.Sprintf("Solving %v problem using:\n", problemName)
    s += fmt.Sprintf("%v particles, with knn=%v and %v dimensions\n",
    p.particles, p.knn, p.dimensions)
    s += fmt.Sprintf("Maximum %v iterations and fitness goal: %v\n", p.maxIterations, p.fitnessGoal)
    s += fmt.Sprintf("C1: %v and C2: %v\n", p.c1, p.c2)
    s += strings.Repeat("-", 60) + "\n"
    return s
}

func PlotParticles(p *gnuplot.Plotter, particles []*Particle, iteration int) {
    p.ResetPlot()

    // Calculate the x- and y-coords
    xcoords := make([]float64, len(particles))
    ycoords := make([]float64, len(particles))
    zcoords := make([]float64, len(particles))

    for i, particle := range particles {
        fmt.Println(particle.positions[0])
        switch currentProblem.dimensions {
            case 1:
                xcoords[i] = particle.positions[0]
            case 2:
                xcoords[i] = particle.positions[0]
                ycoords[i] = particle.positions[1]
            case 3:
                xcoords[i] = particle.positions[0]
                ycoords[i] = particle.positions[1]
                zcoords[i] = particle.positions[2]
        }
    }
    fmt.Println("---")

    t := fmt.Sprintf("Iteration %v", iteration)
    switch currentProblem.dimensions {
        case 1:
            zero_coords := make([]float64, len(particles))
            p.PlotXY(xcoords, zero_coords, t)
        case 2:
            p.PlotXY(xcoords, ycoords, t)
        case 3:
            p.PlotXYZ(xcoords, ycoords, zcoords, t)
    }
    time.Sleep(1000 * time.Millisecond)
}

func CreatePersistentPlotter(lowerbound, upperbound int) *gnuplot.Plotter {
    // Create a plotter
    plotter,err := gnuplot.NewPlotter("", true, false)
    if err != nil {
        log.Fatal(err)
    }

    // Set ranges
    plotter.CheckedCmd("set noautoscale")
    plotter.CheckedCmd(fmt.Sprintf("set xrange [%v:%v]", lowerbound, upperbound))
    plotter.CheckedCmd(fmt.Sprintf("set yrange [%v:%v]", lowerbound, upperbound))

    return plotter
}

func main() {
    // Run flags
    var problem = flag.String("problem", "circle", "Which problem to solve")
    var runs = flag.Int("runs", 1, "How many runs")
    var knapsackFile = flag.String("knapsackFile", "", "Where to look for packages")
    var maxIterations = flag.Int("maxIterations", 0, "Where to look for packages")
    var plotting = flag.Bool("plotting", false, "Enable gnuplotting")
    var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

    // Particle flags
    var dimensions = flag.Int("dimensions", 0, "How many dimensions to solve for")
    var fitnessGoal = flag.Float64("fitnessGoal", 0.0, "Which fitnessGoal to go for")
    var numParticles = flag.Int("numParticles", 0, "How many particles we want")
    var verbose = flag.Int("verbose", 0, "Verbosity level")
    var maxWeight = flag.Float64("maxWeight", 0.0, "Max weight in knapsack problem")
    var maxVolume = flag.Float64("maxVolume", 0.0, "Max volume in the knapsack container")
    var knn = flag.Int("knn", 0, "KNN to use in problem (increases running time)")
    var c1 = flag.Float64("c1", 0, "C1 value to use. Local attraction")
    var c2 = flag.Float64("c2", 0, "C2 value to use. Global attraction")
    var inertia = flag.Bool("inertia", false, "Should we use a slowly decreasing inertia function")

    flag.Parse()

    if (*cpuprofile != "") {
        f, err := os.Create(*cpuprofile)
        if (err != nil) {
            log.Fatal(err)
        }
        pprof.StartCPUProfile(f)
        defer pprof.StopCPUProfile()
    }

    currentProblem = getProblem(*problem, *knapsackFile)

    if (*dimensions != 0){
        currentProblem.dimensions = *dimensions
    }
    if *fitnessGoal != 0 {
        currentProblem.fitnessGoal = *fitnessGoal
    }
    if *numParticles != 0 {
        currentProblem.particles = *numParticles
    }
    if *maxWeight != 0 {
        currentProblem.maxWeight = *maxWeight
        currentProblem.fitnessGoal = -(*maxWeight)
    }
    if *knn != 0 {
        currentProblem.knn = *knn
    }
    if *maxVolume != 0 {
        currentProblem.maxVolume = *maxVolume
    }
    if *maxIterations != 0 {
        currentProblem.maxIterations = *maxIterations
    }
    if *c1 != 0 {
        currentProblem.c1 = *c1
    }
    if *c2 != 0 {
        currentProblem.c2 = *c2
    }
    if !(*inertia) {
        // We default to not having a inertia function.
        currentProblem.inertia = func (iteration int) float64 { return 1.0 }
    }
    if *plotting {
        if (currentProblem.dimensions > 3) {
            *plotting = false
        }
    }

    fmt.Printf(getProblemStatement(*problem, currentProblem))

    for i := 0; i < *runs; i++ {
      // Seed the ranom generator
      rand.Seed(time.Now().UTC().UnixNano())

      // Plotter
      var plotter *gnuplot.Plotter
      if *plotting {
        plotter = CreatePersistentPlotter(13000, 13000)
        defer plotter.Close()
      }

      // Create some particles in random spots
      particles := make([]*Particle, currentProblem.particles)

      // Fill them with values (particles with random coordinates and velocity
      for i := 0; i < len(particles); i++ {
          p := CreateParticle()
          particles[i] = p
      }

      // Array holding the best scores seen globally for each dimension.
      bestGlobalScore := math.MaxFloat64
      var bestParticle *Particle

      /* Loop as long as conditions are not met */
      maxIterations := currentProblem.maxIterations
      fitnessGoal := currentProblem.fitnessGoal

      iteration := 0
      runStart := time.Now()
      for iteration < maxIterations && bestGlobalScore > fitnessGoal {

          // From the different positions in the swarm, we choose the global 
          // best in each dimension.
          var globalBestScores []float64

          if (currentProblem.knn == 0) {
            globalBestScores = GetBestGlobalScores(particles)
          }

          for _, particle := range particles {
              currentProblem.score(particle)
          }

          // Now we update all particles in the swarm based on the best particle
          w := currentProblem.inertia(iteration)
          for _, particle := range particles {

              if (particle.bestFitness < bestGlobalScore) {
                bestGlobalScore = particle.bestFitness
                bestParticle = particle
              }

              if (currentProblem.knn > 0) {
                  localParticles := particle.getNeighbours(particles, currentProblem.knn, "manhattan")
                  globalBestScores = GetBestGlobalScores(localParticles)
              }

              /* Calculate new values in each dimension */
              for dim := 0; dim < currentProblem.dimensions; dim++ {
                currVel := particle.velocities[dim]
                currPos := particle.positions[dim]
                particleBestPos := particle.bestPositions[dim]

                // Get the best global score seen in this dimension
                bestDimensionalGlobalScore := globalBestScores[dim]
                newVel := newVelocity(w, currVel, currPos, particleBestPos, bestDimensionalGlobalScore)
                currentProblem.updateVelocity(particle, dim, newVel)
                currentProblem.updatePosition(particle, dim, newVel)
              }
          }
          if *verbose == 1 {
            fmt.Printf("%v, %.5f\n", iteration, bestGlobalScore)
          }
          if (*plotting) {
            PlotParticles(plotter, particles, iteration)
          }
          iteration++
          if (*verbose == 2) {
              if (iteration % 2 == 0) {
                  avgTime := time.Now().Sub(runStart).Seconds() / float64(iteration)
                  fmt.Printf("\nIteration: %v. Avg secs per iteration: %.2f\n", iteration, avgTime)
                  fmt.Printf("Best fitness: %v\n", bestGlobalScore)
                  if (*problem == "knapsack"  || *problem == "knapsack-volume") {
                    fmt.Printf("Best weight: %v\n", bestParticle.bestWeight)
                    fmt.Printf("Best value: %v\n", bestParticle.bestValue)
                    if (*problem == "knapsack-volume") {
                        fmt.Printf("Best volume: %v\n", bestParticle.bestVolume)
                    }
                    fmt.Printf("%v\n", binaryArray(bestParticle.bestPositions, currentProblem.threshold))

                  } else {
                    fmt.Printf("Best value: %v\n", bestParticle.bestPositions)
                  }
              }

          }
      }
      if (*plotting) {
        PlotParticles(plotter, particles, iteration)
      }
      fmt.Printf("\nFound solution on iteration %v\n", iteration)
      fmt.Printf("Best fitness: %v\n", bestGlobalScore)
      if (*problem == "knapsack" || *problem == "knapsack-volume") {
          fmt.Printf("Best weight: %v\n", bestParticle.bestWeight)
          fmt.Printf("Best value: %v\n", bestParticle.bestValue)
          if (*problem == "knapsack-volume") {
            fmt.Printf("Best volume: %v\n", bestParticle.bestVolume)
          }
          fmt.Printf("Best particle position %v\n", binaryArray(bestParticle.bestPositions, currentProblem.threshold))

      } else {
          fmt.Printf("Best particle position %v\n", bestParticle.bestPositions)
      }
  }
}
